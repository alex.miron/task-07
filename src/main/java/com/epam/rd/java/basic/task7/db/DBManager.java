package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;



public class DBManager extends Configs {
    //INSERT
	private static final String INSERT_USER = "INSERT INTO users (login) VALUES(?)";
	private static final String INSERT_TEAM = "INSERT INTO teams (name) VALUES(?)";
	private static final String INSERT_USERS_TEAMS = "INSERT INTO users_teams (user_id,team_id) VALUES(?, ?)";

	//FIND
	private static final String FIND_ALL_TEAMS = "SELECT * FROM teams";
	private static final String FIND_ALL_USERS = "SELECT * FROM users";
	private static final String FIND_GET_TEAM_BY_USER = "SELECT * FROM users_teams WHERE user_id = ?";
	private static final String FIND_TEAM_BY_ID = "SELECT * FROM teams WHERE id = ?";
	private static final String GET_USER = "SELECT * FROM users WHERE login = ?";
	private static final String GET_TEAM = "SELECT * FROM teams WHERE name = ?";
	//DELETE
	private static final String DELETE_TEAM = "DELETE FROM teams WHERE id = ?";
	private static final String DELETE_USERS = "DELETE FROM users WHERE id = ?";
	//UPDATE
	private static final String UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";

	private static DBManager instance;
//	private Connection dbConnection;

	public static synchronized DBManager getInstance() {
		if(instance == null){
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
//		String connectionString = "jdbc:mysql://" + dbHost + ":" + dbPort + "/"
//				+ dbName;
//		try {
//			dbConnection = DriverManager.getConnection(connectionString,
//					dbUser,dbPass);
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
	}

//	public Connection getDbConnection() throws ClassNotFoundException, SQLException {
//		return dbConnection;
//	}

	public List<User> findAllUsers() throws DBException {

		List<User> users = new ArrayList<>();
		try {
			Connection connection = DriverManager.getConnection(getURL());
			PreparedStatement statement =connection.prepareStatement(FIND_ALL_USERS);
			ResultSet rs = statement.executeQuery();
			while (rs.next()){
				users.add(getUserSet(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try {
			Connection connection = DriverManager.getConnection(getURL());
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, user.getLogin());
			//preparedStatement.executeUpdate();
			int rows = preparedStatement.executeUpdate();
			if(rows == 0){
				throw  new SQLException();
			}
			ResultSet rs = preparedStatement.getGeneratedKeys();
			if(rs.next()){
				user.setId(rs.getInt(1));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		for (User user: users) {
			if(user == null){
				return false;
			}
			try {
				Connection connection = DriverManager.getConnection(getURL());
				PreparedStatement preparedStatement = connection.prepareStatement(DELETE_USERS);
				preparedStatement.setInt(1,user.getId());
				preparedStatement.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		User user = User.createUser(login);
		try {
			Connection connection = DriverManager.getConnection(getURL());
			PreparedStatement preparedStatement = connection.prepareStatement(GET_USER);
			preparedStatement.setString(1,login);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				user.setId(resultSet.getInt("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = Team.createTeam(name);
		try {
			Connection connection = DriverManager.getConnection(getURL());
			PreparedStatement preparedStatement = connection.prepareStatement(GET_TEAM);
			preparedStatement.setString(1,name);
			ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()){
				team.setId(resultSet.getInt("id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try {
			Connection connection = DriverManager.getConnection(getURL());
			PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_TEAMS);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()){
				teams.add(getTeamSet(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try {
			Connection connection = DriverManager.getConnection(getURL());
			PreparedStatement preparedStatement = connection.prepareStatement(INSERT_TEAM,
					Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1, team.getName());
           int rows = preparedStatement.executeUpdate();

			if(rows==0){
				throw new SQLException();
			}
			ResultSet rs = preparedStatement.getGeneratedKeys();
			if(rs.next()){
				team.setId(rs.getInt(1));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if(user == null) throw new DBException("",new NullPointerException());
		try {
			Connection connection = DriverManager.getConnection(getURL());
			connection.setAutoCommit(false);
			try {
				PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USERS_TEAMS);
				for (Team team : teams) {
					if (team == null) {
						connection.rollback();
						throw new DBException("",new NullPointerException());
					}
					preparedStatement.setString(1, String.valueOf(user.getId()));
					preparedStatement.setString(2, String.valueOf(team.getId()));
					preparedStatement.executeUpdate();
				}
			}catch (SQLException throwables){
				connection.rollback();
				throw new DBException("",throwables);
			}
			connection.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		try {
			Connection connection = DriverManager.getConnection(getURL());
			PreparedStatement preparedStatement = connection.prepareStatement(FIND_GET_TEAM_BY_USER);
			preparedStatement.setInt(1,user.getId());
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()){
				teams.add(getTeamByUser(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		if(team == null) return false;
		try {
			Connection connection = DriverManager.getConnection(getURL());
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TEAM);
			preparedStatement.setInt(1,team.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		try {
			Connection connection = DriverManager.getConnection(getURL());
			PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TEAM);
			preparedStatement.setString(1,team.getName());
			preparedStatement.setInt(2,team.getId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}
	private String getURL(){
		String URL = null;
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("app.properties"));
			URL = properties.getProperty("connection.url");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return URL;
	}
	private User getUserSet(ResultSet rs) throws SQLException{
		User user = User.createUser(rs.getString("login"));
		user.setId(rs.getInt("id"));
		return user;
	}
	private Team getTeamSet(ResultSet rs) throws SQLException{
		Team team = Team.createTeam(rs.getString("name"));
		team.setId(rs.getInt("id"));
		return team;
	}
	private Team getTeamByUser(ResultSet rs) throws SQLException{
		try( 	Connection connection = DriverManager.getConnection(getURL());
				PreparedStatement preparedStatement = connection.prepareStatement(FIND_TEAM_BY_ID);) {
			preparedStatement.setInt(1,rs.getInt("team_id"));
			ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			return getTeamSet(resultSet);
		}
	}
}
